var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.json());


app.all('*', function(req, res, next) {
	res.header('Access-Control-Allow-Origin', req.headers.origin);
	res.header('Access-Control-Allow-Credentials', true);
	res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type');

	next();
});

app.use(function logJsonParseError(err, req, res, next) {
	if (err.status === 400 && err.name === 'SyntaxError' && err.body) {
		console.log('JSON body parser error!');
		console.log('send valid JSON');
		res.send('send valid JSON');
	} else {
		next();
	}
});

app.get('/', function(req, res) {
	res.send(' World!');
});

var entry = require('./controller/entry');

app.get('/entry', entry.getAllUsersPost);
app.get('/getPostId', entry.getPostId);
app.post('/authorisation', entry.authorisation);
app.post('/registration', entry.registration);
app.post('/addpost', entry.addPost);
app.delete('/deletepost', entry.deletepost);
app.post('/addcomment', entry.addcomment);
app.delete('/deletecomment', entry.deletecomment);
app.post('/getcomments', entry.getcomments);
app.post('/changeratingpost', entry.changeratingpost);
app.post('/getratingId', entry.getratingId);
app.post('/getPostId', entry.getPostId);


app.listen(3000, function() {
	console.log('Example app listening on port 3000!');
});